#include <stdio.h>

#include "Fraction.h"

int main(void) {
    Fraction fa, fb;
    char op;
    char c;
    printf("Entrer deux fractions : ");
    scanf("%d/%d %d/%d", &fa.num, &fa.den, &fb.num, &fb.den);

    while ((c = getchar()) != '\n' && c != EOF) { } // On "nettoie" le stdin, pour éviter de sauter le scanf suivant

    printf("Entrez une opération : ");
    scanf("%c", &op);

    switch (op) {
        case '+':
            addFraction(fa,fb);
            break;
        case '-':
            subFraction(fa,fb);
            break;
        case '*':
            mulFraction(fa,fb);
            break;
        case '/':
            divFraction(fa,fb);
            break;
        default:
            printf("Opération non supportée (+,-,*,/)\n");
            break;
    }
    
    return 0;
}
