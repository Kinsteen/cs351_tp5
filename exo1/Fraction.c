#include <stdio.h>

#include "Fraction.h"

int pgcd(int a, int b) {
    int r;

    while (b > 0) {
        r = a % b;
        a = b;
        b = r;
    }

    return a;
}

void addFraction(Fraction f1, Fraction f2) {
    int a = f1.num * f2.den + f2.num * f1.den;
    int b = f1.den * f2.den;
    int p = pgcd(a, b);

    printf("Le résultat est %d/%d\n", a/p,b/p);
}

void subFraction(Fraction f1, Fraction f2) {
    int a = f1.num * f2.den - f2.num * f1.den;
    int b = f1.den * f2.den;
    int p = pgcd(a, b);

    printf("Le résultat est %d/%d\n", a/p,b/p);
}

void mulFraction(Fraction f1, Fraction f2) {
    int a = f1.num * f2.num;
    int b = f1.den * f2.den;
    int p = pgcd(a, b);

    printf("Le résultat est %d/%d\n", a/p,b/p);
}

void divFraction(Fraction f1, Fraction f2) {
    int a = f1.num * f2.den;
    int b = f1.den * f2.num;
    int p = pgcd(a, b);

    printf("Le résultat est %d/%d\n", a/p,b/p);
}
