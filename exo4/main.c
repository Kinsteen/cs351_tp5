#include <stdio.h>
#include <stdlib.h>

#include "listeTableau.h"

int main()
{
    /* déclaration du tableau contenant la liste */
    element maListe[TAILLEMAX];

    creerListeVide(maListe);

    insererElement(11, maListe);
    insererElement(12, maListe);
    insererElement(13, maListe);
    insererElement(14, maListe);
    insererElement(15, maListe);
    insererElement(16, maListe);
    insererElement(17, maListe);
    insererElement(18, maListe);

    supprimerElement(3, maListe);
    supprimerElement(6, maListe);
    supprimerElement(7, maListe);

    afficherListe(maListe);

    compacterListe(maListe);
    printf("Après compactage :\n");
    afficherListe(maListe);

    return 0;
}
