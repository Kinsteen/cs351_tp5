/*********************************************************************/
/* Realisation d'une liste chainee dans un tableau                   */
/* ioannis.parissis@grenoble-inp.fr                                  */
/* Sep 2012                                                          */
/*********************************************************************/
#include "listeTableau.h"

void creerListeVide(liste l) {
    l[0].suivant = 0;
    for (int i = 1; i < TAILLEMAX; i++) {
        l[i].suivant = -1;
    }
}

int elementLibre(liste l) {
    for (int i = 0; i < TAILLEMAX; i++) {
        if (l[i].suivant == -1) {
            return i;
        }
    }

    return -1; // Normalement jamais atteint
}

void insererElement(int x, liste l) {
    int i = l[0].suivant;
    int found = 0;

    while (!found) {
        if (l[i].suivant == 0) { // Si on trouve la fin de la liste
            int nIdx = elementLibre(l); // On trouve un élement libre
            l[i].suivant = nIdx; // On assigne le nouvel index
            l[nIdx].valeur = x; // On remplit le nouveau bloc
            l[nIdx].suivant = 0;
            found = 1;
        } else { // Sinon on continue de parcourir la liste
            i = l[i].suivant;
        }
    }
}

void supprimerElement(int i, liste l) {
    int j = l[0].suivant;
    int found = 0;

    while (!found) {
        if (l[j].suivant == i) { // Si le bloc suivant est celui qu'on veut supprimer
            int toDelIdx = l[j].suivant; // On enregistre son idx
            l[j].suivant = l[toDelIdx].suivant; // On prend l'index du bloc d'après qu'on veut supprimer, et on l'assigne au suivant du bloc actuel
            l[toDelIdx].valeur = 0; // On supprime le bloc
            l[toDelIdx].suivant = -1;
            found = 1; // On arrete la boucle
        } else {
            j = l[j].suivant;
        }
    }
}

void afficherListe(liste l) {
    int i = l[0].suivant;
    
    do {
        printf("%d:%d\n", i, l[i].valeur);
        i = l[i].suivant;
    } while (i != 0);
}

void compacterListe(liste l) {
    int i = l[0].suivant;
    int done = 0;

    while (!done) {
        int next = l[i].suivant; // On récupère l'index du bloc suivant
        int newIdx = elementLibre(l);

        if (newIdx < next) { // Si un bloc libre est plus proche que l'index du bloc suivant
            l[newIdx].valeur = l[next].valeur; // On écrit sur le nouveau bloc les données de l'ancien bloc
            l[newIdx].suivant = l[next].suivant;
            l[i].suivant = newIdx; // On remplace bien l'ancien index par le nouveau
            l[next].suivant = -1; // On supprime l'ancien bloc à la main
        }

        i = l[i].suivant; // On continue de parcourir la chaine

        if (next == 0) { // On a fini la chaine, on arrete la boucle
            done = 1;
        }
    }
}
