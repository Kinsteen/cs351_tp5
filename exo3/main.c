#include <stdio.h>
#include <stdlib.h>

#include "pile.h"

int calculatePostFix(char* exp) {
    pileInt pile = creerPile(); // On crée la pile des opérandes

    int i = 0;
    while (exp[i] != '\0') { // On parcourt l'expression postfix caractère par caractère
        if (exp[i] >= '0' && exp[i] <= '9') { // Si c'est un chiffre, on le met dans la pile
            empiler(pile, exp[i] - '0'); // On convertit l'ascii en int
        } else { // C'est une opération
            int b = depiler(pile); // On récupère les deux opérandes, en dépilant
            int a = depiler(pile);

            switch (exp[i]) { // Puis on rempile le résultat de l'opération
                case '+':
                    empiler(pile, a + b);
                    break;
                case '-':
                    empiler(pile, a - b);
                    break;
                case '*':
                    empiler(pile, a * b);
                    break;
                case '/':
                    empiler(pile, a / b);
                    break;
            }
        }

        i++;
    }

    return depiler(pile); // A la fin, il restera un seul élement dans la pile, et c'est le résultat
}

int main()
{
    char expression[] = "42+5*67-/";

    int result = calculatePostFix(expression);
    printf("Le résultat est : %d\n", result);

    return 0;
}
