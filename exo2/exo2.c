#include <stdio.h>
#include <stdlib.h>

#include "exo2.h"

int main(void) {
    Polynome p1 = {                                       createTerme(2,2), createTerme(1,1),                   createTerme(0, -1) };
    Polynome p2 = { createTerme(3, 7), createTerme(1, 3), createTerme(4,2),                   createTerme(6,0), createTerme(0, -1) };
    Polynome res;

    addPolynomes(p1,p2,res);
    // Avec ces deux polynomes, on a tout les cas de tests :
    // - un polynome est plus grand que l'autre
    // - certains termes ont les memes degrés, d'autres sont seuls
    // - les deux indices vont bouger indépendamment : l'un va avancer tout seul, l'autre aussi, et ils avanceront en meme temps pour certains termes

    printPolynome(res);

    return 0;
}

void printPolynome(Polynome p) {
    int i = 0;

    while (p[i].degre >= 0) {
        printf("%.2fx^%d", p[i].coeff, p[i].degre);
        i++;
        if (p[i].degre >= 0) // Permet d'eviter un plus en fin de ligne
            printf(" + ");
    }

    printf("\n");
}

Terme createTerme(float coeff, int degre) { // On utilise ici une sorte de constructeur, pour faciliter la construction d'un polynôme
    Terme t = {coeff, degre};
    return t;
}


void addPolynomes(Polynome p1, Polynome p2, Polynome res) {
    int i = 0;
    int j = 0;
    int r = 0;

    while (p1[i].degre >= 0 || p2[j].degre >= 0) { // Tant que les deux polynomes n'ont pas été parcourus en entier
        if (p1[i].degre > p2[j].degre) { // Le degré du premier polynome est plus grand
            res[r].coeff = p1[i].coeff; // Il n'y a pas de somme a faire
            res[r++].degre = p1[i].degre;
            i++; // On fait avancer l'indice du premier polynôme, tant qu'on arrive pas sur un coefficient égal.
        } else if (p1[i].degre < p2[j].degre) { // Pareil ici, mais inversé
            res[r].coeff = p2[j].coeff;
            res[r++].degre = p2[j].degre;
            j++;
        } else { // Les degrés sont égaux, donc il faut additionner et avancer les deux indices
            res[r].coeff = p1[i].coeff + p2[j].coeff;
            res[r++].degre = p1[i].degre;
            i++;
            j++;
        }
    }

    res[r].coeff = 0;
    res[r].degre = -1; // On force le prochain coeff pour "terminer" le polynome
}
