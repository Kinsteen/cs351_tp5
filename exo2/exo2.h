#ifndef EXO2_H
#define EXO2_H

#define NBMAX 100

typedef struct {
    float coeff; /* coefficient du terme*/
    int degre; /* degré du terme*/
} Terme;

typedef Terme Polynome[NBMAX];

void printPolynome(Polynome p);
void addPolynomes(Polynome p1, Polynome p2, Polynome res);
Terme createTerme(float coeff, int degre);

#endif